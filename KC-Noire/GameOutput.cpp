// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "GameOutput.h"
#include "Utils.h"
#include <cstdlib>

// TODO: Add text file output, so the player has a log of their play.
namespace Go
{
    void Out( std::string text )
    {
        std::cout << text;
    }

    void OutLine( std::string text )
    {
        std::cout << text << std::endl;
    }

    void Type( std::string text )
    {
        for ( int i = 0; i < text.size(); i++ )
        {
            std::cout << text[i];
            Utils::Sleep( 10000 );
        }
    }

    void DisplayHorizBar()
    {
        for ( int i = 0; i < 80; i++ )
        {
            std::cout << "-";
        }
    }

    void ClearScreen()
    {
        #ifdef __MINGW32__
            system("cls");
        #endif

        #ifdef __GNUC__
            system("clear");
        #endif
    }
}
