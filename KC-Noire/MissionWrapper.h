// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _MISSIONWRAPPER
#define _MISSIONWRAPPER

#include <string>

#include "BaseLuaWrapper.h"

class MissionWrapper : public BaseLuaWrapper
{
    public:
    void Init(lua_State* state);

    std::string GetNameOfCurrentMission();
    std::vector<std::string> GetListOfMissions();
    std::string GetMissionIntroduction();
    bool SetCurrentMission(const std::string& key);
    std::string GetBadCommandResponse();
    std::string GetWhatsNextPrompt();
    std::string GetMissionEndFlag();
    std::vector<std::string> GetEndMissionDialog( const std::string& switchVal );
};

#endif
