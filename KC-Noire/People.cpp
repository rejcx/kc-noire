// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "Utils.h"
#include "GameIO.h"
#include "People.h"

void People::Init( lua_State* state, Notebook& notebook )
{
    m_wPeople.Init( state );
    m_ptrNotebook = &notebook;
}

void People::DisplayPeopleOnMap( const std::string& locRoomKey )
{
    std::vector<std::string> lstPeople = m_wPeople.GetListOfPeopleOnMap( locRoomKey );

    GIO::OutLine();
    for ( unsigned int i = 0; i < lstPeople.size(); i++ )
    {
        GIO::OutLine( m_wPeople.GetNameOfPerson( lstPeople[i] ) + " is standing here." );
    }
}

bool People::DisplayPersonDescription( const std::string& personName )
{
    std::string description = m_wPeople.GetDescriptionOfPerson( personName );
    if ( description == "" ) return false;
    GIO::Type( description );
    return true;
}

// TODO: Clean this shit up!
bool People::StartDialogWith( const std::string& personName, const std::string& mapKey )
{
    GIO::ClearScreen();
    GIO::Debug( "StartDialogWith" );
    // Check to make sure what they're trying to talk to is:
    // (A) On the map, and (B) an actual person

    if ( !m_wPeople.PersonExistsAndIsOnMap( personName, mapKey ) )
    {
        return false;
    }

    // const std::string& personName, const std::string& dialogState, const std::string& messageTo
    m_dialogType = TALK;
    m_itemSubject = "";
    std::string userInput;
    while ( m_dialogType != END )
    {
        // Get user input
        if ( m_dialogType != TALK )
        {
            userInput = GetDialogResponse( personName );
            if ( userInput == "Stop" )
            {
                GIO::OutLine( "I tipped my hat at " + personName + " and went on my way." );
                m_dialogType = END;
            }
        }

        #if IN_GAME_DEBUG == 1
        if      ( m_dialogType == TALK )            { GIO::Debug( "State: Talk" ); }
        else if ( m_dialogType == ASK )             { GIO::Debug( "State: Ask" ); }
        else if ( m_dialogType == INTERROGATION )   { GIO::Debug( "State: Interrogation" ); }
        else if ( m_dialogType == EVIDENCE )        { GIO::Debug( "State: Evidence" ); }
        #endif

        GIO::ClearScreen();
        GIO::DisplayHorizBar( "Talking to " + personName );

        // Display Dialog
        switch( m_dialogType )
        {
            case TALK:
                ProcessDialog( personName, "talk", "" );
                break;
            case ASK:
                ProcessDialog( personName, "ask", userInput );
                break;
            case INTERROGATION:
                ProcessDialog( personName, "interrogation", userInput );
                break;
            case EVIDENCE:
                ProcessDialog( personName, "evidence", userInput );
                break;
            default:
                break;
        }
    }

    return true;
}

std::string People::GetDialogResponse( const std::string& personName )
{
    GIO::Debug( "GetDialogResponse" );
    std::vector<std::string> choiceList;
    switch( m_dialogType )
    {
        case ASK:
            // Get notebook entries
            choiceList = m_ptrNotebook->GetNotebookDialogEntries( personName );
            choiceList.push_back( "placeholder=Stop" );
            break;
        case INTERROGATION:
            // Lie, doubt, or trust
            choiceList.push_back("placeholder=Truth");
            choiceList.push_back("placeholder=Doubt");
            choiceList.push_back("placeholder=Lie");
            break;
        case EVIDENCE:
            // Get notebook entries
            choiceList = m_ptrNotebook->GetAllNotebookEntries();
            break;
        default:
            GIO::Debug( "Invalid Dialog Type: \"" + Utils::IntToString( m_dialogType ) + "\"" );
            break;
    }

    GIO::OutLine();

    if ( m_dialogType == EVIDENCE )
    {
        GIO::DisplayHorizBar( "Choose your evidence!" );
    }
    else if ( m_dialogType == ASK )
    {
        GIO::DisplayHorizBar( "Choose a question!" );
    }
    else
    {
        GIO::DisplayHorizBar( "Choose a response..." );
    }

    // TODO: Cleanup
    for ( unsigned int i = 0; i < choiceList.size() - 1; i++ )
    {
        std::string choiceName = "";
        if ( i % 2 == 0 )
        {
            choiceName = "\n";
            choiceName += Utils::IntToString( i+1 ) + ": " + Utils::SplitString( choiceList[i], '=' )[1];
            int charCount = 40 - choiceName.size();
            for ( int j = 0; j < charCount; j++ )
            {
                choiceName += " ";
            }
        }
        else
        {
            choiceName += Utils::IntToString( i+1 ) + ": " + Utils::SplitString( choiceList[i], '=' )[1];
        }
        GIO::Out( choiceName );
    }

    // Add "stop" option:
    int lastElement = choiceList.size() - 1;
    GIO::OutLine();
    GIO::OutLine( Utils::IntToString( lastElement+1 ) + ": " + Utils::SplitString( choiceList[lastElement], '=' )[1] );

    GIO::OutLine();
    GIO::OutLine( "\n\tSo I thought the best response would be..." );
    GIO::Out( "\t>> " );
    unsigned int input = GIO::GetUserInputInt();
    while ( input <= 0 || input > choiceList.size() )
    {
        GIO::OutLine("\tInvalid choice, choose again:");
        GIO::Out( "\t>> " );
        input = GIO::GetUserInputInt();
    }
    if ( m_dialogType == ASK ) { m_itemSubject = Utils::SplitString( choiceList[input-1], '=' )[1]; }
    return Utils::SplitString( choiceList[input-1], '=' )[1];
}

void People::ProcessDialog( const std::string& personName, const std::string& state, const std::string& message )
{
    GIO::Debug( "ProcessDialog" );
    // Display dialog; ret[0] is the state, and everything after is dialog.
    std::vector<std::string> ret = m_wPeople.GetDialog( personName, state, message, m_itemSubject );

    if ( ret.size() == 0 )
    {
        GIO::Debug( "Dialog returned nothing" );
        GIO::Type( personName + ":\t I don't know anything about that." );
        GIO::OutLine( "\n\nPress Enter..." );
        Utils::WaitForEnter();
        m_dialogType = ASK;
    }
    else
    {
        for ( unsigned int i = 1; i < ret.size(); i++ )
        {
            GIO::OutLine();
            GIO::Type( ret[i] );
        }
        GIO::OutLine();

        if      ( Utils::ToLower(ret[0]) == "talk" )            { m_dialogType = TALK; }
        else if ( Utils::ToLower(ret[0]) == "ask" )             { m_dialogType = ASK; }
        else if ( Utils::ToLower(ret[0]) == "interrogation" )   { m_dialogType = INTERROGATION; }
        else if ( Utils::ToLower(ret[0]) == "evidence" )        { m_dialogType = EVIDENCE; }
        else if ( Utils::ToLower(ret[0]) == "end" )             { m_dialogType = END; }
    }
}

bool People::PersonExistsAndIsOnMap( const std::string& personName, const std::string& mapKey )
{
    return m_wPeople.PersonExistsAndIsOnMap( personName, mapKey );
}

// EOF
