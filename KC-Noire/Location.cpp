// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "Location.h"
#include "Utils.h"
#include "GameIO.h"

#include <string>
#include <iostream>

void Location::Init(lua_State* state)
{
    m_wLoc.Init(state);
}

void Location::DisplayLocationInfo()
{
    GIO::Out( "Location: " + m_wLoc.GetNameOfCurrentLocation() );
    GIO::Out( "\tArea: " + m_wLoc.GetNameOfCurrentRoom() );
    GIO::OutLine();
    GIO::OutLine( m_wLoc.GetDescriptionOfCurrentRoom() );
    GIO::OutLine();

    std::vector<std::string> neighbors = GetNeighborRoomInfo();
    for ( unsigned int i = 0; i < neighbors.size(); i++ )
    {
        GIO::OutLine( neighbors[i] );
    }
}

std::vector<std::string> Location::GetNeighborRoomInfo()
{
    GIO::Debug( "GetNeighborRoominfo" );
    std::vector<std::string> neighbors = m_wLoc.GetListOfNeighbors(); // direction=roomkey
//    if ( neighbors[0] == "CAR" )
//    {
//        // TODO: Handle in better area than "Get Neighbor Room Info".
//        // Erase car flag, not needed anymore
//        neighbors.erase( neighbors.begin() );
//        HandleCarTransportation( neighbors );
//    }
//    else
//    {
        for ( unsigned int i = 0; i < neighbors.size(); i++ )
        {
            std::string direction = Utils::SplitString(neighbors[i], '=')[0];
            std::string roomKey = Utils::SplitString(neighbors[i], '=')[1];
            neighbors[i] = "To the " + direction + " there was " + roomKey + ".";
        }
//    }

    return neighbors;
}

bool Location::HandleCarTransportation()
{
    GIO::Debug( "HandleCarTransportation" );
    std::vector<std::string> neighbors = m_wLoc.GetListOfNeighbors(); // direction=roomkey
    neighbors.erase( neighbors.begin() );

    GIO::DisplayHorizBar( "Choose a location to drive to" );
    GIO::OutLine();

    // List locations
    for ( unsigned int i = 0; i < neighbors.size(); i++ )
    {
        std::string roomKey = Utils::SplitString(neighbors[i], '=')[1];
        GIO::OutLine( Utils::IntToString(i+1) + ": " + roomKey );
    }

    // Get Input
    GIO::OutLine( "\n\tI decided to go to..." );
    GIO::Out( "\t>> " );
    unsigned int input = GIO::GetUserInputInt();

    while ( input <= 0 || input > neighbors.size() )
    {
        GIO::OutLine("\tInvalid choice, choose again:");
        GIO::Out( "\t>> " );
        input = GIO::GetUserInputInt();
    }

    // Go to new location
    std::string locationName = Utils::SplitString( neighbors[input-1], '=' )[1];
    GIO::Debug( "Move to : " + locationName );
    return MoveToLocationByName( locationName );
}

bool Location::MoveToLocationByName( const std::string locName )
{
    return m_wLoc.DriveToLocation( locName );
}

bool Location::Move(const std::string& direction)
{
    GIO::Debug( "Move" );
    bool result = m_wLoc.Move(direction);

    GIO::Debug( "Current room: " + m_wLoc.GetNameOfCurrentRoom() );
    if ( m_wLoc.GetNameOfCurrentRoom() == "My Car" )
    {
        GIO::Debug( "It's a car, do special shit." );
        result = HandleCarTransportation();
    }

    return result;
}

std::string Location::GetLocationRoomKey()
{
    return m_wLoc.GetLocationRoomKeyOfCurrent();
}

void Location::DisplayLocationInfoBrief()
{
    std::string desc = m_wLoc.GetDescriptionOfCurrentRoom();
    GIO::Type( desc );
    GIO::OutLine();
    std::vector<std::string> neighbors = GetNeighborRoomInfo();
    for ( unsigned int i = 0; i < neighbors.size(); i++ )
    {
        GIO::Type( neighbors[i] );
        GIO::OutLine();
    }
}

// EOF
