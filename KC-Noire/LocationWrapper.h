// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _LOCATIONWRAPPER
#define _LOCATIONWRAPPER

#include <string>

#include "BaseLuaWrapper.h"

class LocationWrapper : public BaseLuaWrapper
{
    public:
    void Init(lua_State* state);

    std::string GetLocationRoomKeyOfCurrent();
    std::string GetNameOfCurrentLocation();
    std::string GetNameOfLocation(const std::string& locKey);
    std::string GetNameOfCurrentRoom();
    std::string GetNameOfRoom(const std::string& roomKey, const std::string& locKey = "");
    std::string GetDescriptionOfCurrentRoom();
    std::string GetNeighborKey(const std::string& direction);
    std::vector<std::string> GetListOfNeighbors();
    std::vector<std::string> GetListOfAvailableLocations();
    bool DriveToLocation(const std::string& locName);
    bool Move(const std::string& direction);
};

#endif
