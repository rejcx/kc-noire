// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _LUADEBUGWRAPPER
#define _LUADEBUGWRAPPER

#include <string>
#include "BaseLuaWrapper.h"

class LuaDebugWrapper : public BaseLuaWrapper
{
    public:
    void Init(lua_State* state);
    void DoLuaCommand();
};

#endif
