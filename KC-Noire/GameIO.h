// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _GAMEOUTPUT
#define _GAMEOUTPUT

#include <iostream>
#include <fstream>

// Gameplay output will use these functions so I can do any necessary
// formatting or other things later.

#define IN_GAME_DEBUG 0

class GIO
{
    public:
    static void Init();
    static void Close();
    static void InitIOLog();
    static void CloseIOLog();
    static void Out( std::string text );
    static void OutLine( std::string text = "" );
    static void Type( const std::string& text );
    static void Debug( const std::string& text );
    static void Err( const std::string& text );
    static void DisplayHorizBar( std::string header = "" );
    static void ClearScreen();
    static std::string GetUserInput();
    static int GetUserInputInt();
    static void SetTypeSpeed( int ms );

    private:
    static int m_TypeSpeedMs;
    static std::ofstream m_OutStream;
    static std::ofstream m_DebugStream;
};

#endif
