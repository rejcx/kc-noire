// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "LocationWrapper.h"
#include <iostream>
#include "Utils.h"
#include "GameIO.h"

void LocationWrapper::Init(lua_State* state)
{
    BaseLuaWrapper::Init(state, "locations");
}

// ** BIND LUA FUNCTIONS ** //

std::string LocationWrapper::GetNameOfCurrentLocation()
{
    GIO::Debug( "GetNameOfCurrentLocation()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "loc_GetNameOfCurrent" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " L1" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string LocationWrapper::GetNameOfLocation(const std::string& locKey)
{
    GIO::Debug( "GetNameOfLocation(" + locKey + ")" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "loc_GetNameOf", locKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " L2" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string LocationWrapper::GetLocationRoomKeyOfCurrent()
{
    GIO::Debug( "GetLocationRoomKeyOfCurrent()" );
    std::string location;
    try { location = luabind::call_function<std::string>( ptrState, "loc_GetKeyOfCurrent" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " L3" ); }

    std::string room;
    try { room = luabind::call_function<std::string>( ptrState, "room_GetKeyOfCurrent" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " L3" ); }

    GIO::Debug( "Result " + location + "." + room );
    return location + "." + room;
}


std::string LocationWrapper::GetNameOfCurrentRoom()
{
    GIO::Debug( "GetNameOfCurrentRoom()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "room_GetNameOfCurrent" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " R1" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string LocationWrapper::GetNameOfRoom(const std::string& roomKey, const std::string& locKey/*=""*/)
{
    GIO::Debug( "GetNameOfRoom()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "room_GetNameOf", roomKey, locKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " R2" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string LocationWrapper::GetDescriptionOfCurrentRoom()
{
    GIO::Debug( "GetDescriptionOfCurrentRoom()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "room_GetDescriptionOfCurrent" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " R3" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string LocationWrapper::GetNeighborKey(const std::string& direction)
{
    GIO::Debug( "GetNeighborKey(" + direction + ")" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "room_GetNeighborToThe", direction ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " R4" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::vector<std::string> LocationWrapper::GetListOfNeighbors()
{
    GIO::Debug( "GetListOfNeighbors()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "room_GetNeighborNameList" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " R5" ); }
    std::vector<std::string> lstNeighborKeys = Utils::SplitString(result, '|');
    GIO::Debug( "Result " + result );
    return lstNeighborKeys;
}

bool LocationWrapper::Move(const std::string& direction)
{
    GIO::Debug( "Move(" + direction + ")" );
    bool result = false;
    try { result = luabind::call_function<bool>( ptrState, "room_Move", direction ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " R6" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::vector<std::string> LocationWrapper::GetListOfAvailableLocations()
{
    GIO::Debug( "GetListOfAvailableLocations()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "car_GetAvailableLocationList" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " C1" ); }
    std::vector<std::string> lstNeighborKeys = Utils::SplitString(result, '|');
    GIO::Debug( "Result " + result );
    return lstNeighborKeys;
}

bool LocationWrapper::DriveToLocation(const std::string& locName)
{
    GIO::Debug( "DriveToLocation(" + locName + ")" );
    bool result = false;
    try { result = luabind::call_function<bool>( ptrState, "car_GoToLocation", locName ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " C2" ); }
    GIO::Debug( "Result " + result );
    return result;
}

// EOF
