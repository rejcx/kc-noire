// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _NOTEBOOKWRAPPER
#define _NOTEBOOKWRAPPER

#include <string>

#include "BaseLuaWrapper.h"

class NotebookWrapper : public BaseLuaWrapper
{
    public:
    void Init(lua_State* state);

    bool AddItemToNotebook( const std::string& entryTypeAndKey, const std::string& entryName );
    std::vector<std::string> GetNotebookContents();
    std::vector<std::string> GetNotebookContentsForDialog( const std::string& talkingToName );
    bool IsItemInNotebook( const std::string& entryTypeAndKey );
};

#endif
