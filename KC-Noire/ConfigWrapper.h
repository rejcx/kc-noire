// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _CONFIGWRAPPER
#define _CONFIGWRAPPER

#include <string>
#include "BaseLuaWrapper.h"

class ConfigWrapper : public BaseLuaWrapper
{
    public:
    void Init(lua_State* state);

    int GetTypeSpeed();
};

#endif
