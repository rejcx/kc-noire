// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "BaseLuaWrapper.h"
#include "GameIO.h"
#include "Utils.h"

void BaseLuaWrapper::Init(lua_State* state, std::string scriptName)
{
    ptrState = state;
    std::string scriptPath = "content/scripts/" + scriptName + ".lua";
    GIO::Debug( "Loading script: " + scriptPath );

    int loadState = luaL_dofile( ptrState, scriptPath.c_str() );
    if ( loadState != 0 )
    {
        GIO::Err( "ERROR - Trying to load script \"" + scriptPath + "\" failed. \nReturn code is " + Utils::IntToString( loadState ) );
    }
}

void BaseLuaWrapper::Init(lua_State* state)
{
    ptrState = state;
}
