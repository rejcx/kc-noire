-- KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

-- The Notebook is different from the People and Items scripts.
-- It is not a physical object you will see in the world,
-- it will be brought up via a menu, or during a dialog with a person.
-- Once items are examined, they will be added to the Notebook,
-- so you can reference these items as you are interrogating a person.

notebook = {}

-- notebook array only stores keys of items it stores; item.key, character.key, location.key

-- FUNCTIONALITY --

function book_AddItemToNotebook(typeAndKey, itemName)
    if book_IsItemInNotebook( typeAndKey ) == false then
        lastIndex = book_GetSizeOfNotebook()
        notebook[lastIndex+1] = {}
        notebook[lastIndex+1]["key"] = typeAndKey
        notebook[lastIndex+1]["name"] = itemName
        notebook[lastIndex+1]["talkedAboutTo"] = {}
        return true
    else
        return false
    end
end

function book_RemoveItemFromNotebook(typeAndKey)
    index = 0
    for key, value in pairs( notebook ) do
        index = index + 1
        if value["key"] == typeAndKey then
            table.remove( notebook, index )
        end
    end
end

function book_debug_PrintTable()
    for k, v in pairs( notebook ) do
        for a, b in pairs( v.talkedAboutTo ) do
            print ( " * " .. a )
        end
    end
end

function book_GetNotebookContents()
    notebookList = "|"
    for k, v in pairs(notebook) do
        notebookList = notebookList .. v.key .. "=" .. v.name .. "|"
    end
    return notebookList
end

function book_GetNotebookLocations()
    notebookList = "|"
    for k, v in pairs(notebook) do
        if string.find( v.key, "location" ) then
            notebookList = notebookList .. "placeholder" .. "=" .. v.name .. "|"
        end
    end
    return notebookList
end

function book_GetNotebookDialogContents( talkingToName )
    --book_debug_PrintTable()
    notebookList = "|"
    for entryIdx, entry in pairs(notebook) do
        displayThis = true
        for person in pairs ( entry["talkedAboutTo"] ) do
            if string.lower( person ) == string.lower(talkingToName) then
                displayThis = false
            end
        end

        if displayThis then notebookList = notebookList .. entry.key .. "=" .. entry.name .. "|" end
    end
    return notebookList
end

function book_GetItemFromKey(typeAndKey)
    for k, v in pairs(notebook) do
        if string.lower( v.key ) == string.lower( typeAndKey ) then
            return v
        end
    end
    return nil
end

function book_GetItemFromName(name)
    for k, v in pairs(notebook) do
        if string.lower( v.name ) == string.lower( name ) then
            return v
        end
    end
    return nil
end

function book_CheckOffDialogItem( itemName, personName )
    itemObj = book_GetItemFromName( itemName )
    itemObj["talkedAboutTo"][personName] = true
end

function book_IsItemInNotebook(typeAndKey)
    for k, v in pairs(notebook) do
        if v.key == typeAndKey then
            return true
        end
    end
    return false
end

function book_GetSizeOfNotebook()
    count = 0
    for _ in pairs(notebook) do count = count + 1 end
    return count
end
