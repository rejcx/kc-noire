-- KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

-- Locations are where you, the main character, are currently at.
-- There are different items and people in varying locations,
-- and only certain locations will be available during any given mission.
-- Each Location is made up of Rooms, so for example, you could be
-- at the location "House", and you can go into Rooms, "Living Room",
-- "Dining Room", and "Bed Room".
-- To get from House to Work, you will need to use a car, which
-- will give you a menu of Location choices.  Some Locations
-- will not be in the Notebook until you examine the right Item.

-- FUNCTIONALITY --

-- ******************* LOCATION FUNCTIONS --
function loc_GetNameOfCurrent()
    return loc_GetNameOf(curLocation)
end

function loc_GetNameOf(locKey)
    locName = location[locKey].m_name
    if locName ~= nil then      return locName      else return "" end
end

function loc_GetKeyOfCurrent()
    return curLocation
end

-- ******************* ROOM FUNCTIONS --
function room_Move(direction)
    roomMoveTo = room_GetNeighborKeyToThe(direction)
    if roomMoveTo ~= nil then
        if roomMoveTo == "CAR.CAR" then
            curRoom = "CAR"
            curLocation = "CAR"
            return true
        else
            curRoom = roomMoveTo
            return true
        end
    end
    return false
end

function room_GetNameOfCurrent()
    return room_GetNameOf(curRoom, curLocation)
end

function room_GetNameOf(roomKey, locKey)
    if locKey == nil then locKey = curLocation end
    roomName = location[locKey].m_rooms[roomKey].m_name
    if roomName ~= nil then    return roomName     else return "" end
end

function room_GetDescriptionOfCurrent()
    desc = location[curLocation].m_rooms[curRoom].m_description
    if desc ~= nil then         return desc         else return "" end
end

function room_GetNeighborKeyToThe(direction)
    room = room_GetCurrentRoomTable()
    neighborRoomKey = room.m_neighbors[direction]
    if neighborRoomKey ~= nil then      return neighborRoomKey      else return nil end
end

function room_GetNeighborNameList()
    room = room_GetCurrentRoomTable()

    if room.m_name == "My Car" then
        neighborKeyList = car_GetAvailableLocationList()
    else
        neighborKeyList = "|"
        for dirKey, locKey in pairs(room.m_neighbors) do
            if string.find( locKey, ".", 1, true ) then
                -- In another location
                periodIdx = string.find( locKey, ".", 1, true )
                loc = string.sub( locKey, 1, periodIdx-1 )
                room = string.sub( locKey, periodIdx+1 )
                neighborKeyList = neighborKeyList .. dirKey .. "=" .. location[loc].m_rooms[room].m_name .. "|"
            else
                -- Local to the current location
                neighborKeyList = neighborKeyList .. dirKey .. "=" .. location[curLocation].m_rooms[locKey].m_name .. "|"
            end
        end
    end

    return neighborKeyList
end

function room_GetCurrentRoomTable()
    room = location[curLocation].m_rooms[curRoom]
    if room ~= nil then     return room     else return {} end
end

function room_GetKeyOfCurrent()
    return curRoom
end

-- ******************* CAR FUNCTIONS --

function car_GetAvailableLocationList()
    locs = "|CAR"
    locs = locs .. book_GetNotebookLocations()
    return locs
end

function car_GoToLocation(locName)
    for k, v in pairs( location ) do
        if string.lower( v.m_name ) == string.lower( locName ) then
            -- Move to starting room of this location
            curLocation = k
            curRoom = v.m_startRoomKey
            return true
        end
    end
    return false
end

-- LOCATIONS --

location = {}

location.CAR = {
    m_name = "My Car",
    m_startRoomKey = "CAR",
    m_rooms = {
        CAR = {
            m_name = "My Car",
            m_description = "Sittin' in my comfortable drivers seat"
        }
    }
}

location.kenagypark = {
    m_name = "Kenagy Park",
    m_startRoomKey = "parkingLot",
    m_rooms = {
        parkingLot = {
            m_name = "Parking Lot",
            m_description = "I was standing in the shady parking lot of Kenagy Park.\nThe sun was bright and the breeze... cool.  A good day for tennis or fishing.",
            m_neighbors = {
                west = "grassyField",
                south = "CAR.CAR"
            }
        },
        grassyField = {
            m_name = "Grassy Field",
            m_description = "I found myself in a grassy clearing away from the tennis court, \nwhere people have set up a few barbecue smokers. The area was taped off, though,\nbecause of the unscenic dead guy.\n\nBohne was standing next to Richardson's smoker, where Richardson was found after\n tasting some of his recipe, then soon collapsing.",
            m_neighbors = {
                east = "parkingLot"
            }
        }
    }
}

location.benettis = {
    m_name = "Benetti's Coffee Experience",
    m_startRoomKey = "frontLounge",
    m_rooms = {
        frontLounge = {
            m_name = "Front Lounge",
            m_description = "I came to the front lounge of the coffee shop. There were pasteries and coffee here!",
            m_neighbors = {
                east = "backLounge",
                west = "CAR.CAR"
            }
        },
        backLounge = {
            m_name = "Back Lounge",
            m_description = "There were more chairs in the back, and interesting \"art\" on the wall.",
            m_neighbors = {
                west = "frontLounge"
            }
        }
    }
}

location.tacobell = {
    m_name = "Taco Bell",
    m_startRoomKey = "eastEntrance",
    m_rooms = {
        eastEntrance = {
            m_name = "East Entrance",
            m_description = "As I had stepped into the Taco Bell, my feet stuck to the floor.\n\t Grumblin' and freeing myself, I looked around:\n\t Kind of a modern-styled building. Interesting, for fast-food.",
            m_neighbors = {
                east = "CAR.CAR",
                south = "orderingQueue",
                north = "bathroom",
                west = "outsideDining"
            }
        },
        orderingQueue = {
            m_name = "Ordering Queue",
            m_description = "There was nobody in line at the Taco Bell",
            m_neighbors = {
                north = "eastEntrance"
            }
        },
        bathroom = {
            m_name = "Men's Bathroom",
            m_description = "The Taco Bell bathroom was probably the worst I had ever smelled.\n\t I couldn't place the smell, neither, it was just BAD.\n\t There was the usual sign for employees to wash their hands,\n\t but nothing else really special in there.",
            m_neighbors = {
                south = "eastEntrance"
            }
        },
        outsideDining = {
            m_name = "West Outside Dining Area",
            m_description = "If ya couldn't stand the stink of the indoors,\n\t you could always park yourself outdoors, by the highway.\n\t There were a couple tables outside, grass, sidewalk, sky.\n\t Nothin' special.",
            m_neighbors = {
                east = "eastEntrance"
            }
        }
    }
}

curLocation = "kenagypark"
curRoom = location[curLocation].m_startRoomKey

