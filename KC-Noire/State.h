// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _STATE
#define _STATE

#include <luabind.hpp>
#include <lua.hpp>
#include <lualib.h>

#include <string>

class State
{
    public:
    virtual bool Init(lua_State* state) = 0;
    virtual bool MainLoop() = 0;
    virtual std::string Name() = 0;

    protected:
    lua_State* m_state;
};

#endif
