// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "StateManager.h"
#include "GameIO.h"

int GIO::m_TypeSpeedMs = 0;
std::ofstream GIO::m_OutStream;
std::ofstream GIO::m_DebugStream;

int main()
{
    GIO::Init();

    StateManager mgrStates;
    mgrStates.MainLoop();

    GIO::Close();

    return 0;
};
