// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _NOTEBOOK
#define _NOTEBOOK

#include <string>
#include <vector>

#include "NotebookWrapper.h"

class Notebook
{
    public:
    void Init(lua_State* state);
    void DisplayNotebookLocations();
    void DisplayNotebookEvidence();
    void DisplayNotebookContents();
    void AddItemToNotebook( const std::string& entryTypeAndKey, const std::string& entryName );
    std::vector<std::string> GetAllNotebookEntries();
    std::vector<std::string> GetNotebookDialogEntries( const std::string& talkingToName );

    private:
    NotebookWrapper m_wNotebook;
};

#endif

