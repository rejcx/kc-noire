// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "MenuState.h"
#include "Utils.h"
#include "GameIO.h"
#include <string>
#include <fstream>

bool MenuState::Init(lua_State* state)
{
    m_state = state;
    m_wConfig.Init( state );
    // TODO: Clean where config is loaded
    GIO::SetTypeSpeed( m_wConfig.GetTypeSpeed() );

    return true;
}

bool MenuState::MainLoop()
{
    DisplayTitleScreen();

    // Wait for keypress

    GIO::OutLine( "This game is best viewed in an 80x50 console window.  Press Enter..." );
    Utils::WaitForEnter();

    GIO::ClearScreen();

    GIO::OutLine( "K.C. Noire version " + Utils::SoftwareVersion() );
    GIO::OutLine( "By Rachel J. Morris, Spring 2012" );
    GIO::OutLine( "www.moosader.com" );

    GIO::DisplayHorizBar();
    GIO::OutLine( "\nChoose a Mission:" );
    int missionCount = m_ptrMission->DisplayMissionList();
    GIO::OutLine( Utils::IntToString(missionCount+1) + ".\tQuit" );

    int choice = -1;
    while ( choice < 0 || choice > missionCount+1 )
    {
        GIO::Out( ">> " );
        choice = Utils::StringToInt( GIO::GetUserInput() );
    }

    // Quit
    if ( choice == missionCount+1 )     { return false; }

    // Select mission
    if ( !m_ptrMission->SelectMission( choice ) )
    {
        GIO::Debug( "Error, cannot find mission " + choice );
    }
    m_ptrMission->DisplayMissionIntroduction();
    GIO::OutLine();
    GIO::OutLine( "Press ENTER to begin mission..." );
    Utils::WaitForEnter();

    return true;
}

std::string MenuState::Name()
{
    return "Menu";
}

void MenuState::DisplayTitleScreen()
{
    std::ifstream infile( "content/titlescreen.txt" );
    std::string buffer;
    std::vector<bool> titleScr;

    int w=0, h=0;
    while ( infile >> buffer )
    {
        if ( buffer == "WIDTH" )
        {
            infile >> buffer;
            w = Utils::StringToInt( buffer );
        }
        else if ( buffer == "HEIGHT" )
        {
            infile >> buffer;
            h = Utils::StringToInt( buffer );
        }
        else
        {
            titleScr.push_back( Utils::StringToInt( buffer ) );
        }
    }

    infile.close();

    for ( unsigned int i = 0; i < titleScr.size(); i++ )
    {
        if ( i % w == 0 ) { GIO::OutLine(); }

        if ( titleScr[i] == 1 )
        {
            // This is probably bad. TODO: Research how to output ascii properly.
            GIO::Out( "█" );
        }
        else
        {
            GIO::Out( " " );
        }
    }
    GIO::Out( "\n\n" );
}

void MenuState::SetMissionPtr( Mission& msn )
{
    m_ptrMission = &msn;
}



