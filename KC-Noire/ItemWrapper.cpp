// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "ItemWrapper.h"
#include <iostream>
#include <string>
#include <vector>
#include "Utils.h"
#include "GameIO.h"

void ItemWrapper::Init(lua_State* state)
{
    BaseLuaWrapper::Init(state, "items");
}

std::string ItemWrapper::GetDescriptionOfItem( const std::string& itemName )
{
    GIO::Debug( "GetDescriptionOfItem(" + itemName + ")" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "item_GetDescriptionFromName", itemName ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " I1" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string ItemWrapper::GetNameOfItem( const std::string& itemKey )
{
    GIO::Debug( "GetNameOfItem(" + itemKey + ")" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "item_GetName", itemKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " I2" ); }
    GIO::Debug( "Result " + result );
    return result;
}

bool ItemWrapper::SetItemCollected( const std::string& itemName )
{
    GIO::Debug( "SetItemCollected(" + itemName + ")" );
    bool result = false;
    try { result = luabind::call_function<bool>( ptrState, "item_SetItemCollected", itemName ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " I3" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::vector<std::string> ItemWrapper::GetListOfItemsOnMap( const std::string& mapKey )
{
    GIO::Debug( "GetListOfItemsOnMap(" + mapKey + ")" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "item_GetItemsOnMap", mapKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " I4" ); }
    GIO::Debug( "Result " + result );
    std::vector<std::string> lstItems = Utils::SplitString(result, '|');
    return lstItems;
}

std::string ItemWrapper::GetItemKeyFromName( const std::string& itemName )
{
    GIO::Debug( "GetItemKeyFromName(" + itemName + ")" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "item_GetKeyFromName", itemName ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " I5" ); }
    GIO::Debug( "Result " + result );
    return result;
}

bool ItemWrapper::ItemExistsAndIsOnMap( const std::string& itemName, const std::string& mapKey )
{
    GIO::Debug( "ItemExistsAndIsOnMap(" + mapKey + ")" );
    bool result = false;
    try { result = luabind::call_function<bool>( ptrState, "item_ItemExistsAndIsOnMap", itemName, mapKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " I6" ); }
    GIO::Debug( "Result " + result );
    return result;
}

// EOF
