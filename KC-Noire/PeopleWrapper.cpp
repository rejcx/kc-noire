// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "PeopleWrapper.h"
#include <iostream>
#include <string>
#include <vector>
#include "Utils.h"
#include "GameIO.h"

void PeopleWrapper::Init(lua_State* state)
{
    BaseLuaWrapper::Init(state, "people");
}

std::vector<std::string> PeopleWrapper::GetListOfPeopleOnMap( const std::string& mapKey )
{
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "people_GetPeopleOnMap", mapKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " P1" ); }
    std::vector<std::string> lstItems = Utils::SplitString(result, '|');
    GIO::Debug( "People on this map: " + result );
    return lstItems;
}

std::string PeopleWrapper::GetNameOfPerson( const std::string& personKey )
{
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "people_GetName", personKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " P2" ); }
    return result;
}

std::string PeopleWrapper::GetDescriptionOfPerson( const std::string& personName )
{
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "people_GetDescriptionFromName", personName ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " P3" ); }
    return result;
}

std::vector<std::string> PeopleWrapper::GetDialog( const std::string& personName, const std::string& dialogState, const std::string& messageTo, const std::string& itemName )
{
    GIO::Debug( "dialog_GetDialog - Name: \"" + personName + "\", State: \"" + dialogState + "\", Message: \"" + messageTo + "\" Item: \"" + itemName + "\"" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "dialog_GetDialog", personName, dialogState, messageTo, itemName ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " P4" ); }
    std::vector<std::string> lstItems = Utils::SplitString(result, '|');
    return lstItems;
}

bool PeopleWrapper::PersonExistsAndIsOnMap( const std::string& personName, const std::string& mapKey )
{
    GIO::Debug( "Check if person \"" + personName + "\" is on  map \"" + mapKey + "\"" );
    bool result = false;
    try { result = luabind::call_function<bool>( ptrState, "people_PersonExistsAndIsOnMap", personName, mapKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " P5" ); }
    return result;
}

// EOF
