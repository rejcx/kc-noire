// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _BASELUAWRAPPER
#define _BASELUAWRAPPER

#include <luabind.hpp>
#include <string>
#include <iostream>

class BaseLuaWrapper
{
    public:
    void Init(lua_State* state, std::string scriptName);
    void Init(lua_State* state);
    protected:
    lua_State* ptrState;
};

#endif
