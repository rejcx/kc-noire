// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _MISSION
#define _MISSION

#include "MissionWrapper.h"

class Mission
{
    public:
    void Init( lua_State* state );
    int DisplayMissionList();
    bool SelectMission( int episode );
    void DisplayMissionIntroduction();
    std::string GetBadCommandResponse();
    std::string GetWhatsNextPrompt();
    bool HandleMissionSwitch();

    private:
    MissionWrapper m_wMission;
};

#endif
