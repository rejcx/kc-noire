// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _STATEMANAGER
#define _STATEMANAGER

#include "MenuState.h"
#include "GameState.h"
#include "Mission.h"

class StateManager
{
    public:
    StateManager();
    ~StateManager();
    void Init();
    void MainLoop();

    private:
    lua_State* m_state;
    State* m_currentState;
    // Temp
    MenuState m_menuState;
    GameState m_gameState;
    Mission m_mission;
};

#endif
